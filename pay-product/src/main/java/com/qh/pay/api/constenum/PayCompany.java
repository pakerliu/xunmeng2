package com.qh.pay.api.constenum;

import java.util.HashMap;
import java.util.Map;

import com.qh.common.config.Constant;

/**
 * @ClassName PayCompany
 * @Description 支付公司
 * @Date 2017年11月9日 上午9:44:10
 * @version 1.0.0
 */
public enum PayCompany {
	/**原生支付宝***/
	alipay,
	/**原生微信***/
	wxpay,
	/**银生宝   快捷 网关  代付**/
	ysb,
	/**聚富****/
	jf,
	/**天付宝**/
	tfb,
	/**环迅**/
	hx,
	/**bopay**/
	bopay,
	/**九派***/
	jiupay,
	/**芯付**/
	xinfu,
	/**点芯**/
	dianxin,
	/**芯钱包**/
	xinqianbao,
	/**比可支付**/
	beecloud,
	/**汇收银**/
	hsy,
	/**小天支付(代付)**/
	xiaotian,
	/**摩宝**/
	mobao,
	/**百信达**/
	bxd,
	/**杉德**/
	sd,
	/**汇付宝**/
	hfb,
	/**mofangceo***/
	mofang
	;
	private static final Map<String,String> descMap = new HashMap<>(10);
	
	private static final Map<String,PayCompany> enumMap = new HashMap<>(10);
	
	static{
		descMap.put(alipay.name(), "支付宝-农夫山泉测试账户");
		enumMap.put(alipay.name(), alipay);
		
		descMap.put(wxpay.name(), "微信-农夫山泉测试账户");
		enumMap.put(wxpay.name(), wxpay);
		
		descMap.put(mofang.name(), "mofangceo");
		enumMap.put(mofang.name(), mofang);
	}
	
	/***当前支付公司*******/
	private static final Map<String,String> jfDescMap = new HashMap<>(4);
	static{
		jfDescMap.put(jf.name(), Constant.pay_name);
		enumMap.put(jf.name(), jf);
	}
	
	public static Map<String, String> jfDesc() {
		return jfDescMap;
	}
	
	/***配置支付公司支持的卡类型****/
	private static final Map<String,Integer> companyCardTypeMap = new HashMap<>();
	static{
		companyCardTypeMap.put(jiupay.name(), CardType.savings.id());
		companyCardTypeMap.put(beecloud.name(), CardType.savings.id());
	}
	
	public static Integer companyCardType(String name){
		return companyCardTypeMap.get(name);
	}
	
	/***配置支付公司是否需要绑卡短信****/
	private static final Map<String,Integer> companyBindCardSMSMap = new HashMap<>();
	static{
		companyBindCardSMSMap.put(jiupay.name(), YesNoType.yes.id());
		companyBindCardSMSMap.put(beecloud.name(), YesNoType.not.id());
	}
	
	public static Integer companyBindCardSMS(String name){
		return companyBindCardSMSMap.get(name);
	}
	
	/***配置支付公司是否有重发短信验证码****/
	private static final Map<String,Integer> companyResendSMSMap = new HashMap<>();
	static{
		companyResendSMSMap.put(jiupay.name(), YesNoType.yes.id());
		companyResendSMSMap.put(beecloud.name(), YesNoType.not.id());
	}
	
	public static Integer companyResendSMS(String name){
		return companyResendSMSMap.get(name);
	}
	
	/***配置支付公司代付是否需要银联行号****/
	private static final Map<String,Integer> companyUnionPayNeedMap = new HashMap<>();
	static{
		companyUnionPayNeedMap.put(jiupay.name(), YesNoType.not.id());
		companyUnionPayNeedMap.put(beecloud.name(), YesNoType.yes.id());
	}
	
	public static Integer companyUnionPay(String name){
		return companyUnionPayNeedMap.get(name);
	}
	
	public static PayCompany payCompany(String name){
		return enumMap.get(name);
		
	}
	
	/****所有通道*****************/;
    private static final Map<String,String> allDescMap = new HashMap<>(16);
    static{
        allDescMap.putAll(descMap);
        allDescMap.putAll(jfDescMap);
    }
	
	/**
     * @Description 返回所有的通道
     * @return
     */
    public static final Map<String,String> all() {
        return allDescMap;
    }
	
	public static Map<String, String> desc() {
		return descMap;
	}
	
}
