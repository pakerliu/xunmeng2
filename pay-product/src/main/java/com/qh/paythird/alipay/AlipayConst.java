package com.qh.paythird.alipay;



/**
 * 
 * @author 梦想达到
 *
 */
public class AlipayConst {

	
	/**
	 * 支付密钥
	 */
	public static final String alipay_privatekey = "alipay_privatekey";
	/**
	 * 支付密钥
	 */
	public static final String alipay_publickey = "alipay_publickey";
	
	/**
	 * 支付请求地址
	 */
	public static final String alipay_gateway = "alipay_gateway";
	/**
	 * 支付通知地址
	 */
	public static final String alipay_notifyurl = "alipay_notifyurl";
	/**
	 * 支付商户号
	 */
	public static final String alipay_merchantCode = "alipay_merchantCode";
	/**
	 * 支付商户号
	 */
	public static final String alipay_appId = "alipay_appId";
	/**
	 * 字符编码
	 */
	public static final String charset = "UTF-8";
	/**
	 * 加密方式
	 */
	public static final String signType = "RSA2";
	
	/**
	 * 返回数据格式
	 */
	public static final String format="json";
	
	

	
}
