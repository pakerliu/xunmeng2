package com.qh.paythird.mofang;

import com.qh.pay.api.constenum.BankCode;

import java.util.HashMap;
import java.util.Map;

public class MofangConst {
    /***统一下单接口**/
    public static final String DIRECT_PAY_TRADE="com.order.Unified.Pay";				
    /***统一下单查询接口**/
    public static final String OPERATEORDER_VIEW="com.order.Trade.Query";				
    /***余额代付下单接口**/
    public static final String PROXY_PAY="com.proxy.Pay.OrderPay";						
    /***余额代付查询接口*/
    public static final String PROXY_QUERY="com.proxy.Pay.OrderQuery";					
    /***可用金额查询接口**/
    public static final String BALANCE_QUERY="com.proxy.Pay.BalanceQuery";
    /***接口类型:1 是裸接口 2 是收银台**/
    public static final String interface_type_1 = "1";
    /***接口类型:1 是裸接口 2 是收银台**/
    public static final String interface_type_2 = "2";
    /***error_code 0为成功***/
    public static final String error_code_succ = "0";
    /***pay_status 1 已支付，0 未支付  支付***/
    public static final String pay_status_succ = "1";
    public static final String pay_status_fail = "0";
    /***账号类型 1 公账 2 私账***/
    public static final String receive_account_type_pri = "2";
    public static final String receive_account_type_pub = "1";
    /***pay_status 再代付当中  支付状态 PAYING,表示受理代付成功***/
    public static final String pay_status_acp_paying = "PAYING";
    /***PAYING 打款中 PAYED 打款成功   REFUND 退款***/
    public static final String pay_status_acp_PAYED = "PAYED";
    public static final String pay_status_acp_REFUND = "REFUND";

    /**微信扫码支付方式，暂未用*/
    public static final String PMT_weixin_qrcode="weixin_qrcode";
    /**qq扫码支付方式***/
    public static final String PMT_qq_qrcode="qq_qrcode";
    /**支付宝支付 扫码**/
    public static final String PMT_alipay_qrcode="alipay_qrcode";
    /**京东支付 扫码**/
    public static final String PMT_jd_qrcode="jd_qrcode";
    /**银联支付 扫码**/
    public static final String PMT_cp_qrcode="cp_qrcode";
    /**银联支付**/
    public static final String PMT_cp_wap="cp_wap";
    /**微信刷卡***/
    public static final String PMT_weixinscan_qrcode = "weixinscan_qrcode";
    /**微信wap***/
    public static final String PMT_weixin_wap = "weixin_wap";
    /**支付宝支付**/
    public static final String PMT_alipay_wap="alipay_wap";
    /**QQwap**/
    public static final String PMT_qq_wap="qq_wap";
    /**京东**/
    public static final String PMT_jd_wap="jd_wap";
    /**网关**/
    public static final String PMT_gateway="gateway";
    /**快捷**/
    public static final String PMT_quick="quick";
    /***支付场景，暂未用**/
    public static final String PAY_TYPE_SWEPT="swept";

    private static final Map<String, String> codeMap = new HashMap<>(32);
    private static final Map<String, String> descMap = new HashMap<>(32);

    public static Map<String,String> bkCode(){
        return codeMap;
    }
    public static Map<String,String> bkDesc(){
        return descMap;
    }
    static {
        codeMap.put(BankCode.ICBC.name(), "ICBC");
        descMap.put(BankCode.ICBC.name(), "中国工商银行");

        codeMap.put(BankCode.ABC.name(), "ABC");
        descMap.put(BankCode.ABC.name(), "中国农业银行");

        codeMap.put(BankCode.BOC.name(), "BOC");
        descMap.put(BankCode.BOC.name(), "中国银行");

        codeMap.put(BankCode.CCB.name(), "CCB");
        descMap.put(BankCode.CCB.name(), "中国建设银行");

        codeMap.put(BankCode.BCOM.name(), "BOCO");
        descMap.put(BankCode.BCOM.name(), "交通银行");

        codeMap.put(BankCode.CMB.name(), "CMB");
        descMap.put(BankCode.CMB.name(), "招商银行");

        codeMap.put(BankCode.GDB.name(), "CGB");
        descMap.put(BankCode.GDB.name(), "广发银行");

        codeMap.put(BankCode.CITIC.name(), "ECITIC");
        descMap.put(BankCode.CITIC.name(), "中信银行");

        codeMap.put(BankCode.CMBC.name(), "CMBC");
        descMap.put(BankCode.CMBC.name(), "中国民生银行");

        codeMap.put(BankCode.CEB.name(), "CEB");
        descMap.put(BankCode.CEB.name(), "中国光大银行");

        codeMap.put(BankCode.PABC.name(), "PINGANBANK");
        descMap.put(BankCode.PABC.name(), "平安银行");

        codeMap.put(BankCode.SPDB.name(), "SPDB");
        descMap.put(BankCode.SPDB.name(), "上海浦东发展银行");

        codeMap.put(BankCode.PSBC.name(), "PSBC");
        descMap.put(BankCode.PSBC.name(), "中国邮政储蓄银行");

        codeMap.put(BankCode.HXB.name(), "HXB");
        descMap.put(BankCode.HXB.name(), "华夏银行");

        codeMap.put(BankCode.CIB.name(), "CIB");
        descMap.put(BankCode.CIB.name(), "兴业银行");

        codeMap.put(BankCode.BOB.name(), "BCCB");
        descMap.put(BankCode.BOB.name(), "北京银行");

        codeMap.put(BankCode.BOS.name(), "SHB");
        descMap.put(BankCode.BOS.name(), "上海银行");

        codeMap.put(BankCode.BRCB.name(), "BJRCB");
        descMap.put(BankCode.BRCB.name(), "北京农村商业银行");

    }



}
