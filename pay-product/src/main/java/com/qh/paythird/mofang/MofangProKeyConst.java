package com.qh.paythird.mofang;

public class MofangProKeyConst {
    /***mofang商户号 mf0000071****/
    public static final String mofang_merchant_no = "mofang_merchant_no";
    /***mofang 商户签约PID 10801710000000902712**/
    public static final String mofang_partner_id = "mofang_partner_id";
    /***mofang 请求地址  测试地址 http://api.mofangceo.com/v2***/
    public static final String mofang_apiUrl = "mofang_apiUrl";
    /***签名类型  SHA1WithRSA(默认)，SHA256WithRSA ***/
    public static final String mofang_algorithm = "mofang_algorithm";
    /***签名类型 RSA(默认)，RSA2 与上面两个参数同步修改***/
    public static final String mofang_sign_type = "mofang_sign_type";
    /***mofang 公钥***/
    public static final String mofang_public_key = "mofang_public_key";
    /***mofang 商户私钥***/
    public static final String mofang_mc_private_key = "mofang_mc_private_key";
    /***mofang 商户公钥***/
    public static final String mofang_mc_public_key = "mofang_mc_public_key";


}
