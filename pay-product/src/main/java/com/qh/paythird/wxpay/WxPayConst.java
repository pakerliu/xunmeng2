package com.qh.paythird.wxpay;



/**
 * 
 * @author 丁志超
 *
 */
public class WxPayConst {

	
 
	public static final String wxpay_appSecret = "wxpay_appSecret";
 
	public static final String wxpay_apiSecret = "wxpay_apiSecret";
	
	/**
	 * 支付创建请求地址
	 */
	public static final String wxpay_gateway = "wxpay_gateway";
	/**
	 * 支付查询请求地址
	 */
	public static final String wxpay_orderquery = "wxpay_orderquery";
	/**
	 * 支付通知地址
	 */
	public static final String wxpay_notifyurl = "wxpay_notifyurl";
	/**
	 * 支付获取用户信息
	 */
	public static final String wxpay_openidurl = "wxpay_openidurl";
	/**
	 * 支付商户号
	 */
	public static final String wxpay_merchantCode = "wxpay_merchantCode";
	/**
	 * APP ID
	 */
	public static final String wxpay_appId = "wxpay_appId";
	/**
	 * 支付商户号
	 */
	public static final String wxpay_mchId = "wxpay_mchId";
	/**
	 * 字符编码
	 */
	public static final String charset = "UTF-8";
	/**
	 * 加密方式
	 */
	public static final String signType = "RSA2";
	
	/**
	 * 返回数据格式
	 */
	public static final String format="json";
	
	

	
}
