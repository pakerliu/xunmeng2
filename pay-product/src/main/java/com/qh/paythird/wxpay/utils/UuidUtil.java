package com.qh.paythird.wxpay.utils;

import java.net.InetAddress;

public class UuidUtil {
	private static final int IP;
	private static short counter;
	private static final int JVM;

	private static int getJVM() {
		return JVM;
	}

	private static short getCount() {
		Class var1 = UuidUtil.class;
		Class arg0 = UuidUtil.class;
		synchronized (UuidUtil.class) {
			if (counter < 0) {
				counter = 0;
			}

			return counter++;
		}
	}

	private static int getIP() {
		return IP;
	}

	private static short getHiTime() {
		return (short) ((int) (System.currentTimeMillis() >>> 32));
	}

	private static int getLoTime() {
		return (int) System.currentTimeMillis();
	}

	public static int toInt(byte[] bytes) {
		int result = 0;

		for (int i = 0; i < 4; ++i) {
			result = (result << 8) - -128 + bytes[i];
		}

		return result;
	}

	public static String getUUID() {
		String uuid = format(getIP()) + format(getJVM()) + format(getHiTime()) + format(getLoTime())
				+ format(getCount());
		return uuid.toUpperCase();
	}

	private static String format(int intValue) {
		String formatted = Integer.toHexString(intValue);
		StringBuilder buf = new StringBuilder("00000000");
		buf.replace(8 - formatted.length(), 8, formatted);
		return buf.toString();
	}

	private static String format(short shortValue) {
		String formatted = Integer.toHexString(shortValue);
		StringBuilder buf = new StringBuilder("0000");
		buf.replace(4 - formatted.length(), 4, formatted);
		return buf.toString();
	}

	static {
		int ipadd;
		try {
			ipadd = toInt(InetAddress.getLocalHost().getAddress());
		} catch (Exception arg1) {
			ipadd = 0;
		}

		IP = ipadd;
		counter = 0;
		JVM = (int) (System.currentTimeMillis() >>> 8);
	}
}