package com.qh.paythird.wxpay.utils;

import java.util.*;

/**
 * @Author: 梦想达到
 */
public class NfPayUtil {
    //定义签名，微信根据参数字段的ASCII码值进行排序 加密签名,故使用SortMap进行参数排序
    public static String createSign(String characterEncoding, SortedMap<String, String> parameters) {
        StringBuffer sb = new StringBuffer();
        Set es = parameters.entrySet();
        Iterator it = es.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String k = (String) entry.getKey();
            Object v = entry.getValue();
            if (null != v && !"".equals(v)
                    && !"sign".equals(k) && !"key".equals(k)) {
                sb.append(k + "=" + v + "&");
            }
        }
        String sign = MD5.MD5Encode(sb.toString(), characterEncoding).toUpperCase();
        return sign;
    }

    public static Map transformRequestMap(Map<String, String[]> requestMap){
        Map resMap = new HashMap();
        Iterator iterator = requestMap.keySet().iterator();
        while (iterator.hasNext()) {
            String k = (String) iterator.next();
            resMap.put(k,requestMap.get(k)[0]);
        }
        return resMap;
    }
}
