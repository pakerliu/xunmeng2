package com.qh.pay.service;

import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.qh.pay.api.Order;
import com.qh.pay.api.constenum.OrderState;
import com.qh.redis.service.RedisUtil;

/**
 * @ClassName PayBaseServiceTest
 * @Description 支付基础类测试
 * @Date 2017年11月22日 下午5:48:22
 * @version 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
@ComponentScan("com.qh.pay")
public class PayBaseServiceTest {
	
	@Autowired
	public PayService payService; 
	
	public static final org.slf4j.Logger logger = LoggerFactory.getLogger(PayBaseServiceTest.class);
	/****
	 * 商户号
	 */
	public final static String merchNo = "SH464853";
	/**
	 * 公钥 --聚富
	 */
	public final static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDCpuJkb8b78/WLJUqXQ21/xkDh4/M32gUaNDdDtS5530EbNa1sT2tQi2q+POX+/32VRMlxf/HvibwVJ2uCCeBdUk19g+gP7pi1H4Oo1pBbAg4j6nJWCKpG+8lQ8LNlejggpcFbNnEiMEkZjNnleOpV9u5NAYA/gVpeXTv55+cCnwIDAQAB";
	/**
	 * 私钥 --聚富
	 */
	public final static String privateKey = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAMKm4mRvxvvz9YslSpdDbX/GQOHj8zfaBRo0N0O1LnnfQRs1rWxPa1CLar485f7/fZVEyXF/8e+JvBUna4IJ4F1STX2D6A/umLUfg6jWkFsCDiPqclYIqkb7yVDws2V6OCClwVs2cSIwSRmM2eV46lX27k0BgD+BWl5dO/nn5wKfAgMBAAECgYAgGRLmCwSeEYmhC2I0dVr+IfwbA9Lcl+gVfRzL3nmugSEkUoKphKMmogbVHgezOABGeBRb0gRcQrMwLdLtm6FSmHc/2C8MekF5WDo8poCzMfuqd99FrzrXBtLrhxRLNp3b0jiW8p/yxDZap1FYGVFi+J2IiQf6VzrD62aPbftJ8QJBAO2M80dyGf0faGaeBDzSV1d6hpL5zEgnlsoBpwOIV1zt2r9ulV9UXbvaXJjlQbxY3QXMvpZGUtG1YSVV29KO4NkCQQDRxQJ93enIrwK6F2Qytptl0AhsiTOV6AttHtot+9Zaqjlj7tPh5J3i6YSbK/c466u6zT70asLY+mfiEIwgadQ3AkBv55tfR9AYXuhWlw/V+xysGAs3R7fFMekl5ltgHaVSILaQwa5aRysU6IwAdtddu9r69XW+4wf2xGQsK2MvsKEZAkAaojnzwzZIrbfZ6thk+/x1C9nV9gGfueuhejZXek84Cb7pqkwqtT06r02P6iAkkP+n4yFoJRoCNKD1+gXgo683AkBMBE7IRI/s3rWPK8v4JfaiQyeBcCtCR4g0h3jfRKZNujT+/LBHIeANyLNt4oE38C0tRA4lrRMhImgJdA8+w+WN";
	/**
	 * 公钥 --商户
	 */
	public final static String mcPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC6jELpPz0NVcpyMQIAddOzbOq3JLl8IRQ05OspTjmnAf4DWx42sRwoES9hgXoJSlw57jXm/cbwSB3efOLvmhhzl/6KNyNfC9Jb5BxTzcY3Np5/0w6dhETjNrUEsT2wuOtVOuQeCL1imdDdXriEy1+Pc1PsAfYKdomPcpUxLNZukwIDAQAB";
	/**
	 * 私钥---商户
	 */
	public final static String mcPrivateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALqMQuk/PQ1VynIxAgB107Ns6rckuXwhFDTk6ylOOacB/gNbHjaxHCgRL2GBeglKXDnuNeb9xvBIHd584u+aGHOX/oo3I18L0lvkHFPNxjc2nn/TDp2EROM2tQSxPbC461U65B4IvWKZ0N1euITLX49zU+wB9gp2iY9ylTEs1m6TAgMBAAECgYA/ZWtX2SXmYkFqjT0g6uoHD/y0QMbIaHV1hFZmsy7kksjAWzbpX0cyVqTDfjz660psZYfr+FKjVd7QvaBhzPyBtntq9KxszoHlsrc9IQDboQ/q/8z+aQ7OaxO7yv60JQFUa2tMM73mVLsoXXnzBkJkxp8IXJDsDBJL85Ez6lkYAQJBAPc6nmQ574bbHiKzYhcQ3+8Xz70CfGnPEl2D4owbXFh9pjQArBNGjKdADK9odEDieDd5SFvxbUPGthiY1Fp5Mz8CQQDBKoZ9T029EphegKuMzOt8ugsfs9+/FwRpuAr8vNJ+tdnqM7mxq4BmaR/ZlcKPwnstGP7q4HgVWtlIuyxSNvOtAkBaY7MvX8NEYe6Fr2IonsHQ77rDFxqXsN69FBALO6GqtN3EW46OSZf5OkCpDR6b7nEXjx82hnF1ezxuh8nz/iYPAkACV9y3ym17/KzCgLcUByxYH/2gjdMKA2J3Udc4R6YaCWSyVZxMDnOJSn8raYipq8dIvcPh34U1ZRu7qSGZgbclAkEApZP1LAG2iFq9m2tZ9oGJEbpXLL57NiVs1qPaIXKIYtxF5zJ1m/S1rX35M49QmqnwQ3PRCpFc3CsFj20FjK5nbw==";
	/***
	 * 支付域名
	 */
	public final static String url = "http://localhost:8888/pay/order";
	
	/****
	 * 绑卡域名
	 */
	public final static String card_url = "http://localhost:8888/pay/card";
	
	/**
	 * @Description 设置订单支付中 用于查询
	 * @param orderNo
	 */
	public void setOrderIng(String orderNo) {
		Order order = RedisUtil.getOrder(merchNo, orderNo);
		if(order == null){
			logger.info("订单不存在：{},{}",merchNo,orderNo);
		}
		order.setOrderState(OrderState.ing.id());
		RedisUtil.setOrder(order);
	}
	
	
}
