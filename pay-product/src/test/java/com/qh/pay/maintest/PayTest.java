package com.qh.pay.maintest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qh.pay.api.constenum.OutChannel;
import com.qh.pay.api.utils.RSAUtil;
import com.qh.pay.api.utils.RequestUtils;

public class PayTest {
	/****
	 * 商户号
	 */
	public final static String merchNo = "SH464853";
	/**
	 * 公钥 --聚富
	 */
	public final static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDCpuJkb8b78/WLJUqXQ21/xkDh4/M32gUaNDdDtS5530EbNa1sT2tQi2q+POX+/32VRMlxf/HvibwVJ2uCCeBdUk19g+gP7pi1H4Oo1pBbAg4j6nJWCKpG+8lQ8LNlejggpcFbNnEiMEkZjNnleOpV9u5NAYA/gVpeXTv55+cCnwIDAQAB";
	/**
	 * 私钥 --聚富
	 */
	public final static String privateKey = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAMKm4mRvxvvz9YslSpdDbX/GQOHj8zfaBRo0N0O1LnnfQRs1rWxPa1CLar485f7/fZVEyXF/8e+JvBUna4IJ4F1STX2D6A/umLUfg6jWkFsCDiPqclYIqkb7yVDws2V6OCClwVs2cSIwSRmM2eV46lX27k0BgD+BWl5dO/nn5wKfAgMBAAECgYAgGRLmCwSeEYmhC2I0dVr+IfwbA9Lcl+gVfRzL3nmugSEkUoKphKMmogbVHgezOABGeBRb0gRcQrMwLdLtm6FSmHc/2C8MekF5WDo8poCzMfuqd99FrzrXBtLrhxRLNp3b0jiW8p/yxDZap1FYGVFi+J2IiQf6VzrD62aPbftJ8QJBAO2M80dyGf0faGaeBDzSV1d6hpL5zEgnlsoBpwOIV1zt2r9ulV9UXbvaXJjlQbxY3QXMvpZGUtG1YSVV29KO4NkCQQDRxQJ93enIrwK6F2Qytptl0AhsiTOV6AttHtot+9Zaqjlj7tPh5J3i6YSbK/c466u6zT70asLY+mfiEIwgadQ3AkBv55tfR9AYXuhWlw/V+xysGAs3R7fFMekl5ltgHaVSILaQwa5aRysU6IwAdtddu9r69XW+4wf2xGQsK2MvsKEZAkAaojnzwzZIrbfZ6thk+/x1C9nV9gGfueuhejZXek84Cb7pqkwqtT06r02P6iAkkP+n4yFoJRoCNKD1+gXgo683AkBMBE7IRI/s3rWPK8v4JfaiQyeBcCtCR4g0h3jfRKZNujT+/LBHIeANyLNt4oE38C0tRA4lrRMhImgJdA8+w+WN";
	/**
	 * 公钥 --商户
	 */
	public final static String mcPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC6jELpPz0NVcpyMQIAddOzbOq3JLl8IRQ05OspTjmnAf4DWx42sRwoES9hgXoJSlw57jXm/cbwSB3efOLvmhhzl/6KNyNfC9Jb5BxTzcY3Np5/0w6dhETjNrUEsT2wuOtVOuQeCL1imdDdXriEy1+Pc1PsAfYKdomPcpUxLNZukwIDAQAB";
	/**
	 * 私钥---商户
	 */
	public final static String mcPrivateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALqMQuk/PQ1VynIxAgB107Ns6rckuXwhFDTk6ylOOacB/gNbHjaxHCgRL2GBeglKXDnuNeb9xvBIHd584u+aGHOX/oo3I18L0lvkHFPNxjc2nn/TDp2EROM2tQSxPbC461U65B4IvWKZ0N1euITLX49zU+wB9gp2iY9ylTEs1m6TAgMBAAECgYA/ZWtX2SXmYkFqjT0g6uoHD/y0QMbIaHV1hFZmsy7kksjAWzbpX0cyVqTDfjz660psZYfr+FKjVd7QvaBhzPyBtntq9KxszoHlsrc9IQDboQ/q/8z+aQ7OaxO7yv60JQFUa2tMM73mVLsoXXnzBkJkxp8IXJDsDBJL85Ez6lkYAQJBAPc6nmQ574bbHiKzYhcQ3+8Xz70CfGnPEl2D4owbXFh9pjQArBNGjKdADK9odEDieDd5SFvxbUPGthiY1Fp5Mz8CQQDBKoZ9T029EphegKuMzOt8ugsfs9+/FwRpuAr8vNJ+tdnqM7mxq4BmaR/ZlcKPwnstGP7q4HgVWtlIuyxSNvOtAkBaY7MvX8NEYe6Fr2IonsHQ77rDFxqXsN69FBALO6GqtN3EW46OSZf5OkCpDR6b7nEXjx82hnF1ezxuh8nz/iYPAkACV9y3ym17/KzCgLcUByxYH/2gjdMKA2J3Udc4R6YaCWSyVZxMDnOJSn8raYipq8dIvcPh34U1ZRu7qSGZgbclAkEApZP1LAG2iFq9m2tZ9oGJEbpXLL57NiVs1qPaIXKIYtxF5zJ1m/S1rX35M49QmqnwQ3PRCpFc3CsFj20FjK5nbw==";
	/***
	 * 支付域名
	 */
	public final static String url = "http://localhost:8888/pay/order";
	
	/****
	 * 绑卡域名
	 */
	public final static String card_url = "http://localhost:8888/pay/card";

	public static void main(String[] args) throws Exception {
		String orderNo = creat_order(); 
		//order_query(orderNo);
	}
	
	/**
	 * 支付订单创建
	 * @throws Exception 
	 * */
	public static String creat_order() throws Exception {
		// TODO Auto-generated method stub
		JSONObject jsObj = new JSONObject();
		String reqTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		//商户号
		jsObj.put("merchNo", merchNo);
		//订单号
		jsObj.put("orderNo", reqTime + new Random().nextInt(10000));	
		//支付渠道
		jsObj.put("outChannel", OutChannel.wxpayh5.name());
		if(OutChannel.wy.name().equals(jsObj.get("outChannel"))){
			jsObj.put("bankCode", "CMB");
			jsObj.put("bankName", "招商银行");
		}
		//订单标题 
		jsObj.put("title", "商城网银下单");
		//产品名称
		jsObj.put("product", "产品名称");
		//支付金额 单位 元 
		jsObj.put("amount", String.valueOf(new Random().nextInt(5000)));
		//币种
		jsObj.put("currency", "CNY");
		//前端返回地址
		jsObj.put("returnUrl", "http://www.baidu.com");
		//后台通知地址
		jsObj.put("notifyUrl", "http://www.baidu.com");
		//请求时间
		jsObj.put("reqTime", reqTime);
		//userId
		jsObj.put("userId", "oTZZ0wLl6FJkgX42zM8F_dUjv-bk");
		//对公
		jsObj.put("acctType", 1);
		System.out.println("请求source:" + jsObj.toString());
		byte[] context = RSAUtil.encryptByPublicKey(JSON.toJSONBytes(jsObj), publicKey);
		String sign = RSAUtil.sign(context, mcPrivateKey);
		System.out.println("签名结果：" + sign);
		JSONObject jo = new JSONObject();
		jo.put("sign", sign);
		jo.put("context", context);
		System.out.println("请求参数："+ jo.toJSONString());
		String result = RequestUtils.doPostJson(url, jo.toJSONString());
		System.out.println("请求结果！"+result);
		jo = JSONObject.parseObject(result);
		if("0".equals(jo.getString("code"))){
			sign = jo.getString("sign");
			context = jo.getBytes("context");
			if(RSAUtil.verify(context, publicKey, sign)){
				String source = new String(RSAUtil.decryptByPrivateKey(context, mcPrivateKey));
				System.out.println("解密结果：" + source);
				jo = JSONObject.parseObject(source);
				System.out.println("网银支付链接地址："+ jo.getString("qrcode_url"));
				
			}else{
				System.out.println("验签失败！{}");
			}
		}
		return jsObj.getString("orderNo");
	}
	
	/**订单查询*/
	public static void order_query(String orderNo) throws Exception{
		JSONObject jsObj = new JSONObject();
		//商户号
		jsObj.put("merchNo", merchNo);
		jsObj.put("orderNo", orderNo);
		
		
		byte[] context = RSAUtil.encryptByPublicKey(JSON.toJSONBytes(jsObj), publicKey);
		String sign = RSAUtil.sign(context, mcPrivateKey);
		System.out.println("签名结果：" +sign);
		JSONObject jo = new JSONObject();
		jo.put("sign", sign);
		jo.put("context", context);
		System.out.println("请求参数："+ jo.toJSONString());
		String result = RequestUtils.doPostJson(url + "/query", jo.toJSONString());
		System.out.println("请求结果:"+result);
		jo = JSONObject.parseObject(result);
		if("0".equals(jo.getString("code"))){
			sign = jo.getString("sign");
			context = jo.getBytes("context");
			if(RSAUtil.verify(context, publicKey, sign)){
				String source = new String(RSAUtil.decryptByPrivateKey(context, mcPrivateKey));
				System.out.println("解密结果：" + source);
				jo = JSONObject.parseObject(source);
				System.out.println("订单支付状态:"+jo.getString("orderState"));
				if("1".equals(jo.getString("orderState"))){
					//回调成功通知订单保存
					//payService.orderDataMsg(merchNo, orderNo);
				}
			}else{
				System.out.println("验签失败！{}");
			}
			
		}
		
	}

}
